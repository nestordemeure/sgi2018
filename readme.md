# Eigen follower

## Description

There exists several methods to simulate numerically sound propagation in an inhomogeneous medium.
The method we are going to use here is called *normal modes decomposition*.

The idea is to use the eigenfunctions and eigenvalues of the operator ![f1] which characterize the different waveguides.
The signal will be the sum of different wavepackets each one propagating in the different waveguides.
Thus, the key point is to determine the spectrum of ![f2] in order to compute the different contributions to the propagated signal.
Numerically, the operator is discretized in a matrix whose spectrum can be computed with classical methods.

The aim of this study is to see how the spectrum behaves when the medium is perturbed.
We will described the perturbation using a random parameter ξ (gaussian vector of dimension 2) and thereby the matrix will be seen as a function of ξ.
We want to see how the eigenvalues of a matrix reacts to this perturbation first with a Monte-Carlo approach and then using Polynomial Chaos expansion.

This project contains the code to generate different realizations of the matrix, compute their spectrum and link the different realizations of a given eigenvalue.
It also contains the calibration of a chaos polynomials based metamodel to speed up computations.

[f1]: http://chart.apis.google.com/chart?cht=tx&chl=\mathcal{L}\psi=\frac{d^2\psi}{dz^2}%2B\frac{\omega^2}{c(z)^2}\psi=\lambda\psi
[f2]: http://chart.apis.google.com/chart?cht=tx&chl=\mathcal{L}

## Usage

The project is self contained, you just need [make](https://en.wikipedia.org/wiki/Make_(software)) and [cmake](https://cmake.org/) (which should already be installed on your computer) in order to compile.

To compile, run `cmake .` followed with `make`.

In order to plot the results, you can use and modify the `plot.R` script (you can run it with `Rscript plot.R`) which will output a pdf to the output folder.
Note that you might need to install some R packages (`ggplot2`, `dplyr` and `data.table`) in order to get the `plot.R` script running.

**Warning:** Do not forget to clone the project with its submodules (`git clone --recurse-submodules git@gitlab.com:nestordemeure/sgi2018.git`) or to update them (`git submodule update --init --recursive
`) in order to download [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) with the rest of the project.
If you decide to download Eigen separately, we recommend the [version 3.3](https://gitlab.com/kemen209/eigen/tree/branches/3.3).

## Organisation of the files

The `types` file contains all the types used in the project (one could change them here).

The problem definition is in the `main` file.

The `generator` file contains the code needed to generate the matrix whose eigenvalues are of interest.

The `tools` folder contains function used in the project (such as writing to a csv and reproducing some of matlab's functions).

The `follower` folder contains the code used to follow an eigenvalue when the matrix is changed (via a perturbation or a change in omega).

## Credits

The code was implemented by Nestor Demeure based on a Matlab implementation of chaos polynomials applied to this particular problem by Alexandre Goupy.


