#pragma once

#include "types.h"
#include "tools/matlab.h"


//-------------------------------------------------------------------------------------------------
// Constants

namespace Generator
{
    // by default, do not use constant continuation
    bool use_constant_continuation = false;
}

//-------------------------------------------------------------------------------------------------
// INTERMEDIATE FUNCTIONS

/*
 * The function DM =  chebdif(N,M) computes the differentiation
 * matrices D1, D2, ..., DM on Chebyshev nodes.
 *
 * Input:
 * N:        Size of differentiation matrix.
 * M:        Number of derivatives required (integer).
 * Note:     0 < M <= N-1.
 *
 * Output:
 * DM:       DM(1:N,1:N,ell) contains ell-th derivative matrix, ell=1..M.
 *
 * The code implements two strategies for enhanced
 * accuracy suggested by W. Don and S. Solomonoff in
 * SIAM J. Sci. Comp. Vol. 6, pp. 1253--1268 (1994).
 * The two strategies are (a) the use of trigonometric
 * identities to avoid the computation of differences
 * x(k)-x(j) and (b) the use of the "flipping trick"
 * which is necessary since sin t can be computed to high
 * relative precision when t is small whereas sin (pi-t) cannot.
 * J.A.C. Weideman, S.C. Reddy 1998.
 */
std::pair<Vector, Matrix> chebdif(const int N)
{
    // Indice used for flipping trick.
    int n2 = (N + 1)/2; // ceil(N/2)

    // Compute theta vector.
    Vector k = make_vector(0,N-1);
    Vector th = k*pi/number(N-1);

    // Compute Chebyshev points.
    Vector x = make_vector(N-1,1-N,-2).unaryExpr([N](number x){return STD::sin( pi*x / number(2*(N-1))) ;});

    Matrix T = repmat(th/number(2.),N);
    Matrix DX = (T.transpose()+T).binaryExpr(T.transpose()-T, [](number Tplus, number Tminus){return 2. * STD::sin(Tplus) * STD::sin(Tminus);}); // Trigonometric identity.
    DX.bottomRows(n2) = -flip(DX.topRows(n2));
    DX.diagonal().array() = 1; // Put 1's on the main diagonal of DX.

    // C is the matrix with entries c(k)/c(j)
    Matrix C = toeplitz( k.unaryExpr([](number k){return STD::pow(-1,k);}) );
    C.row(0) *= 2;
    C.row(N-1) *= 2;
    C.col(0) /= 2;
    C.col(N-1) /= 2;

    // Z contains entries 1/(x(k)-x(j)) with zeros on the diagonal.
    Matrix Z = DX.cwiseInverse();
    Z.diagonal().array() = 0;

    // D contains diff. matrices.
    Matrix D = Matrix::Identity(N,N);
    D = Z.cwiseProduct(C.cwiseProduct(repmat(D.diagonal(),N)) - D); // Off-diagonals
    D.diagonal() = - D.rowwise().sum(); // Correct main diagonal of D

    return std::make_pair(x, D);
}

// Compute the sound speed profile
Vector sound_speed(Vector& z_prof, const complexNumber& xi, const bool use_constant_continuation)
{
    // Parameters
    const auto size = z_prof.size();
    const number Zstag  = 0.2;
    const number Beta   = 1;
    const number Vref   = 2;
    const number EpsBD  = 10;
    const number Zref   = 2;
    const number a      = 1 + 0.2*xi.real(); //+0.2*perturbation.points(indGWS,1);
    const number Zj     = 250; //+20*perturbation.points(indGWS,3);
    const number Wj     = 66.7 + 10*xi.imag(); //+10*perturbation.points(indGWS,2);

    // Bussinger-Dyer
    Vector UBD = Vector::Zero(size);
    for(int IndZlocm = 0; IndZlocm < size; IndZlocm++)
    {
        if(z_prof(IndZlocm) > Zstag)
        {
            UBD(IndZlocm) = STD::log(z_prof(IndZlocm)/Zstag) + Beta*(z_prof(IndZlocm)-Zstag);
        }
    }

    // Jet
    Vector UJET;
    if (use_constant_continuation)
    {
        // Jet with constant continuation to avoid tunnel effect
        UJET = Vector::Ones(size);
        for(unsigned int IndZlocm = 0; IndZlocm < size; IndZlocm++)
        {
            if(z_prof(IndZlocm) < Zj)
            {
                UJET(IndZlocm) = STD::exp(-squared((z_prof(IndZlocm)-Zj)/Wj));
            }
        }
    }
    else
    {
        // Jet without constant continuation
        UJET = z_prof.unaryExpr([Zj,Wj](number z_prof){return STD::exp(-squared((z_prof-Zj)/Wj));});
    }

    // Wind profile
    number UBD_ref = STD::log(Zref/Zstag) + Beta*(Zref-Zstag);
    Vector Ulocm = UBD.binaryExpr(UJET, [Vref,EpsBD,UBD_ref,a](number UBD, number UJET){return Vref*(EpsBD+UBD_ref)/UBD_ref*(UBD/(EpsBD+UBD)+a*UJET);});

    // Temperature profile
    const number TG     = 290;
    const number Delta  = 10;
    const number Alpha  = 0.03;
    const number Gamma  = 0; //0.01;

    Vector Tlocm = z_prof.unaryExpr([TG,Delta,Alpha,Gamma](number z_prof){return TG + Delta*(1.-STD::exp(-Alpha*z_prof)) - Gamma*z_prof;});

    // Sound Speed profile
    Vector Clocm = Tlocm.binaryExpr(Ulocm, [](number Tlocm, number Ulocm){return STD::sqrt(1.4*287*Tlocm) + Ulocm;});

    return Clocm;
}

//-------------------------------------------------------------------------------------------------
// OPERATOR DEFINITION

ComplexMatrix matrixEDO(const int N, const complexNumber& omega, const complexNumber& xi, const number k, const bool use_constant_continuation)
{
    Vector x;
    Matrix D;
    std::tie(x, D) = chebdif(N);

    const number zmax = 1e3;
    Vector Z = x.unaryExpr([zmax](number x){return zmax*(1.-x)/2.;});

    Matrix A = Matrix::Identity(N,N);
    number B = 0;
    ComplexVector C = sound_speed(Z, xi, use_constant_continuation).unaryExpr([omega,k](number sz){return squared(omega) / squared(sz) - squared(k);});

    D = -2/zmax*D;
    Matrix DD = D*D;

    ComplexMatrix M = A*DD + B*D;
    M.diagonal() += C;

    // Boundary conditions
    const number a1 = 0;
    const number b1 = 1;
    const number c1 = 0;
    const number aN = 0;
    const number bN = 0;
    const number cN = 1;

    M.row(0) = a1*DD.row(0) + b1*D.row(0);
    M(0,0) += c1;
    M.row(N-1) = aN*DD.row(N-1) + bN*D.row(N-1);
    M(N-1,N-1) += cN;

    return M;
}

/*
 * Compute the discretization of the operator M=operator(omega,N)
 * -omega : freq. at which the computation is done
 *          /!\ omega can have an imaginary part
 * - N    : vertical discretization
 * - M    : discretized operator using spectral collocation
 */
ComplexMatrix generator(const complexNumber omega, const int N, const complexNumber xi = complexNumber(0.,0.), const bool use_constant_continuation = Generator::use_constant_continuation)
{
    ComplexMatrix M0 = matrixEDO(N, omega, xi, 0, use_constant_continuation);

    //ComplexMatrix M1 = matrixEDO(N, omega, xi_a, xi_Wj, 1, use_constant_continuation);
    //ComplexMatrix M = mldivide(M0, M0 - M1);

    // note that M0 - M1 is ID for all values except its first and last lines
    // which are (a1*DD.row(0) + b1*D.row(0)) and (aN*DD.row(N-1) + bN*D.row(N-1))
    // those are 0 for the current boundary conditions
    // meaning that we are basically computing M0^-1 with two zero columns on the sides
    // hence the following method gives the expected matrix but quicker and with improved precision
    ComplexMatrix M = M0.inverse();
    M.col(0).array() = complexNumber(0.,0.);
    M.col(N-1).array() = complexNumber(0.,0.);

    return M;
}

//-------------------------------------------------------------------------------------------------
// EIGENVALUE TRANSFORMATION

// put an eigenvalue in the proper output space
complexNumber transformEigenvalue(const complexNumber eigenvalue)
{
    return number(1.) / std::sqrt(eigenvalue);
}

// take an eigenvalue, omega and compute its phase speed
complexNumber phaseSpeed(const number omega, const complexNumber eigenvalue)
{
    return complexNumber(omega / eigenvalue.real(), eigenvalue.imag());
}

// take a matrix of eigenvalues, omega and compute their phase speed
ComplexMatrix phaseSpeed(const number omega, const ComplexMatrix& eigenvalues)
{
    return eigenvalues.unaryExpr([omega](const complexNumber e){return phaseSpeed(omega, e) ;});
}

//-------------------------------------------------------------------------------------------------