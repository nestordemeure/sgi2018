#pragma once

#include <complex>
#include <vector>
#include <Eigen/Dense>

#ifdef SHAMAN

#include <shaman.h>
#include <shaman/helpers/Shaman_eigen.h>

// types used in the program
typedef Sdouble number;
typedef Sfloat halfNumber;
typedef std::complex<number> complexNumber;
typedef Eigen::SVectorXd Vector;
typedef Eigen::SRowVectorXd RowVector;
typedef Eigen::SVectorXcd ComplexVector;
typedef Eigen::SRowVectorXcd RowComplexVector;
typedef Eigen::SMatrixXd Matrix;
typedef Eigen::SMatrixXcd ComplexMatrix;
typedef std::vector<number> Polynomial;
#define STD Sstd

#else

// types used in the program
typedef double number;
typedef float halfNumber;
typedef std::complex<number> complexNumber;
typedef Eigen::VectorXd Vector;
typedef Eigen::RowVectorXd RowVector;
typedef Eigen::VectorXcd ComplexVector;
typedef Eigen::RowVectorXcd RowComplexVector;
typedef Eigen::MatrixXd Matrix;
typedef Eigen::MatrixXcd ComplexMatrix;
typedef std::vector<number> Polynomial;
#define STD std

#endif
