#pragma once

#include <iostream>
#include "../types.h"
#include "point.h"
#include "follow.h"

namespace Transform
{
    // takes a starting point, the indexes of the target eigenvalues in the starting point and a vector of parameters to generate ending points
    // follows the target from the starting point to each of the ending points, independently
    // returns a matrix with a row per xi and a column per target
    ComplexMatrix linearPath(const Point& startPoint, const std::vector<unsigned int>& startIndexes, const ComplexVector& xiVector,
                             const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        // NOTE:
        // to generate a point from a parameter i, you can use  :
        // Point point(startPoint.omega, startPoint.size, xiVector[i]);
        //
        // to follow several targets from a point to another, you can use :
        // std::vector<unsigned int> indexesResult = Follow::adaptativStep(startPoint, endPoint, startIndexes, distanceThreshold, minstepsize);
        throw std::logic_error("Function not yet implemented.");
    }

    // takes a starting point, the indexes of the target eigenvalues in the starting point and a vector of parameters to generate ending points
    // follows the target from the starting point to each of the ending points, independently
    // returns a matrix with a row per xi and a column per target
    // NOTE: this function exports the phase speeds of all the eigenvalues to a csv file for later analysis
    ComplexMatrix linearPath(const Point& startPoint, const std::vector<unsigned int>& startIndexes, const ComplexVector& xiVector,
                             Csv& outCsv, const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        // NOTE: identical to the previous function but dumps all eigenvalues produced, not just targets, to outCsv
        //
        // to output the eigenvalues of a point, you can use :
        // outCsv.write( (ComplexVector) phaseSpeed(startPoint.omega, point.formatedEigenvalues) );
        //
        // WARNING: you will need some sort of locking mechanism to export values while running in parallel
        throw std::logic_error("Function not yet implemented.");
    }

    // takes a starting point, the indexes of the target eigenvalues in the starting point and a vector of parameters to generate ending points
    // returns a matrix with a row per xi and a column per target
    //
    // to do so, this function builds a minimum spanning tree that is processed inparallel
    // the tree has xi=(0,0) as its root (the starting point) and goes to all the values inside xiVector
    ComplexMatrix treePath(const Point& startPoint, const std::vector<unsigned int>& startIndexes, const ComplexVector& xiVector, Csv& outCsv,
                           const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        // FACULTATIV
        // NOTE: this function can be tricky to implement without using too much memory for large number of points or having an n² complexity
        // you might want to play with building the tree before processing it or processing it as you are building it
        // the fastest implementation gets a bonus
        throw std::logic_error("Function not yet implemented.");
    }
}
